import React, { useState } from "react";
import axios from "axios";
import FileSaver from "file-saver";
import PropTypes from "prop-types";

import UploadInput from "./UploadInput";
import Button from "./Button";

const UploadTranscribe = () => {
    const [file, setFile] = useState(null);
    const [errorMessage, setErrorMessage] = useState("");
    const [fileName, setFileName] = useState("");
    const baseUrl = process.env.REACT_APP_API_BASE_URL || "http://localhost:8000";
    const apiEndpoint = `${baseUrl}/api/transcription`;
    const [loading, setLoading] = useState(false);
    const [outputFormat, setOutputFormat] = useState("srt");
    const [task, setTask] = useState("transcribe");

    const handleFileSelect = (selectedFile) => {
        setFile(selectedFile);
        setFileName(selectedFile.name);
    };

    const handleTranscribe = async () => {
        if (file) {
            setLoading(true);
            const formData = new FormData();
            formData.append("file", file);
            formData.append("output_format", outputFormat);
            formData.append("task", task);
            try {
                const response = await axios.post(apiEndpoint, formData, {
                    responseType: "blob",
                });
                const fileData = new Blob([response.data], { type: "text/srt" });
                FileSaver.saveAs(fileData, `transcription.${outputFormat}`);
                setErrorMessage("");
            } catch (error) {
                console.error(error);
                setErrorMessage("An error occurred while transcribing the file.");
            }
            setLoading(false);
        }
    };

    const handleFormatChange = (event) => {
        setOutputFormat(event.target.value);
    };

    const handleTaskChange = (event) => {
        setTask(event.target.checked ? "translate" : "transcribe");
    };

    return (
        <div className="upload-transcribe">
            <h2>Upload the file to transcribe</h2>
            <p>Supported types: mp3, mp4, wav</p>
            <div className="upload-container">
                <UploadInput onFileSelect={handleFileSelect} />
            </div>
            {errorMessage && <p className="error-message">{errorMessage}</p>}
            {fileName && <p className="success-message">File uploaded successfully!</p>}
            <div className="form-group">
                <label htmlFor="output-format">Choose output format:</label>
                <select
                    id="output-format"
                    className="output-format-select"
                    onChange={handleFormatChange}
                    value={outputFormat}
                >
                    <option value="srt">Subtitles (SRT)</option>
                    <option value="txt">Text</option>
                </select>
            </div>
            <div className="form-group translate-group">
                <label htmlFor="translate-checkbox">Translate to EN</label>
                <input
                    type="checkbox"
                    id="translate-checkbox"
                    onChange={handleTaskChange}
                />
            </div>
            <Button onClick={handleTranscribe} disabled={!file || loading}>
                {loading ? "Transcribing..." : "Transcribe"}
            </Button>
        </div>
    );

};

UploadTranscribe.propTypes = {
    apiEndpoint: PropTypes.string.isRequired,
};

export default UploadTranscribe;