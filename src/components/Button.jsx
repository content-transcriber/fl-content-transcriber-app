import React from "react";
import PropTypes from "prop-types";

const Button = ({ children, onClick, disabled }) => (
    <button className="button" onClick={onClick} disabled={disabled}>
        {children}
    </button>
);

Button.propTypes = {
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
};

Button.defaultProps = {
    disabled: false,
};

export default Button;
