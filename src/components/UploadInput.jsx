import React, { useState } from "react";
import PropTypes from "prop-types";

const UploadInput = ({ onFileSelect }) => {
    const [file, setFile] = useState(null);

    const handleChange = (event) => {
        const selectedFile = event.target.files[0];
        const supportedTypes = ["audio/mpeg", "audio/wav", "video/mp4"];
        if (supportedTypes.indexOf(selectedFile.type) !== -1) {
            setFile(selectedFile);
            onFileSelect(selectedFile);
        } else {
            setFile(null);
        }
    };

    return (
        <div className="upload-input">
            <label htmlFor="file-upload">{file ? file.name : ""}</label>
            <div className="input-container">
                <button className="upload-button"
                    onClick={() => document.getElementById("file-upload").click()}>Upload file...</button>
            </div>
            <input
                type="file"
                id="file-upload"
                accept=".mp3,.mp4,.wav"
                onChange={handleChange}
            />
        </div>
    );
};

UploadInput.propTypes = {
    onFileSelect: PropTypes.func.isRequired,
};

export default UploadInput;
