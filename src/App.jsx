import React from "react";
import UploadTranscribe from "./components/UploadTranscribe";

const App = () => (
  <div className="app">
    <UploadTranscribe />
  </div>
);

export default App;
